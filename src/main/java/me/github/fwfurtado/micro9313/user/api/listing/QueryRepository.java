package me.github.fwfurtado.micro9313.user.api.listing;

import me.github.fwfurtado.micro9313.user.shared.User;

import java.util.Optional;

public interface QueryRepository {
    Optional<User> findById(Long id);
}
