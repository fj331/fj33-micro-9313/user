package me.github.fwfurtado.micro9313.user.shared;

import org.springframework.stereotype.Component;

@Component
public class UserViewConverter {

    public UserView convert(User user) {
        return new UserView(user.getName(), user.getEmail());
    }
}
