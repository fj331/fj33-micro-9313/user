package me.github.fwfurtado.micro9313.user.api.listing;

import me.github.fwfurtado.micro9313.user.shared.UserView;
import me.github.fwfurtado.micro9313.user.shared.UserViewConverter;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
class ListingService {
    private final QueryRepository repository;
    private final UserViewConverter converter;

    ListingService(QueryRepository repository, UserViewConverter converter) {
        this.repository = repository;
        this.converter = converter;
    }

    Optional<UserView> showUserBy(Long id) {
        return repository.findById(id).map(converter::convert);
    }
}
