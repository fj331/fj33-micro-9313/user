package me.github.fwfurtado.micro9313.user.api.registration;

import me.github.fwfurtado.micro9313.user.infra.PasswordFactory;
import me.github.fwfurtado.micro9313.user.shared.User;
import org.springframework.stereotype.Component;

import static me.github.fwfurtado.micro9313.user.api.registration.RegistrationController.UserForm;

@Component
class RegistrationUserFactory {

    private final PasswordFactory passwordFactory;

    RegistrationUserFactory(PasswordFactory passwordFactory) {
        this.passwordFactory = passwordFactory;
    }

    public User convert(UserForm form) {

        var password = passwordFactory.factoryBy(form.getPassword());

        return new User(form.getFullName(), form.getEmail(), password);
    }
}
