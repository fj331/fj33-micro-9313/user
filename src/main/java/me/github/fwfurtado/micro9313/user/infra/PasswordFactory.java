package me.github.fwfurtado.micro9313.user.infra;

import me.github.fwfurtado.micro9313.user.shared.Password;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class PasswordFactory {
    private final PasswordEncoder passwordEncoder;

    public PasswordFactory(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public Password factoryBy(String rawPassword) {
        return Password.fromRawPassword(rawPassword).encodeBy(passwordEncoder);
    }
}
