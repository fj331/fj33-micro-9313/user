package me.github.fwfurtado.micro9313.user.shared;

import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

import static java.util.Objects.requireNonNull;

@Embeddable
public class Password {
    private String password;

    @Transient
    private Boolean encoded;

    protected Password() {
    }

    private Password(String password, Boolean encoded) {
        this.password = password;
        this.encoded = encoded;
    }

    public String getPassword() {
        return password;
    }

    public Password encodeBy(PasswordEncoder encoder) {
        if (encoded) {
            return this;
        }

        var encodedPassword = requireNonNull(encoder).encode(password);

        return new Password(encodedPassword, true);
    }

    public static Password fromRawPassword(String text) {
        return new Password(text, false);
    }

    public static Password fromEncoded(String encoded) {
        return new Password(encoded, true);
    }
}
