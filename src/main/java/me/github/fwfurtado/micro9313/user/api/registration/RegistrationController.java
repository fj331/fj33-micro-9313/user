package me.github.fwfurtado.micro9313.user.api.registration;

import me.github.fwfurtado.micro9313.user.api.UserController;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.util.UriComponentsBuilder;

import static org.springframework.http.ResponseEntity.created;

@UserController
class RegistrationController {


    private final RegistrationService service;

    RegistrationController(RegistrationService service) {
        this.service = service;
    }

    @PostMapping
    ResponseEntity<?> createNewUser(@RequestBody @Validated UserForm form, UriComponentsBuilder uriComponentsBuilder) {
        var id = service.registerUserBy(form);

        var uri = uriComponentsBuilder.path("/{id}").build(id);

        return created(uri).build();
    }

    static class UserForm {
        private String email;
        private String password;
        private String fullName;

        UserForm() {
        }

        public UserForm(String email, String password, String fullName) {
            this.email = email;
            this.password = password;
            this.fullName = fullName;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }

        public String getFullName() {
            return fullName;
        }
    }
}
