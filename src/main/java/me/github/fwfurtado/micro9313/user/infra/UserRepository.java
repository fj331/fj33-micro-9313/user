package me.github.fwfurtado.micro9313.user.infra;

import me.github.fwfurtado.micro9313.user.api.listing.QueryRepository;
import me.github.fwfurtado.micro9313.user.api.registration.RegistrationUserRepository;
import me.github.fwfurtado.micro9313.user.shared.User;
import org.springframework.data.repository.Repository;

interface UserRepository extends Repository<User, Long>, RegistrationUserRepository, QueryRepository {
}
