package me.github.fwfurtado.micro9313.user.api.registration;

import me.github.fwfurtado.micro9313.user.shared.User;

public interface RegistrationUserRepository {
    void save(User newUser);
}
