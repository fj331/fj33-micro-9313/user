package me.github.fwfurtado.micro9313.user.api.registration;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.github.fwfurtado.micro9313.user.api.registration.RegistrationController.UserForm;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;


@Service
class RegistrationService {


    private final RegistrationUserFactory converter;
    private final RegistrationUserRepository repository;
    private final MessageChannel eventChannel;

    RegistrationService(RegistrationUserFactory converter, RegistrationUserRepository repository, @Output(Source.OUTPUT) MessageChannel channel) {
        this.converter = converter;
        this.repository = repository;
        this.eventChannel = channel;
    }

    record CreatedUser(
            @JsonProperty Long id,
            @JsonProperty String name){
        Message<CreatedUser> toMessage() {
            return MessageBuilder.withPayload(this).build();
        }
    }

    Long registerUserBy(UserForm form) {
        var newUser = converter.convert(form);

        repository.save(newUser);

        var event = new CreatedUser(newUser.getId(), newUser.getName());
        eventChannel.send(event.toMessage());

        return newUser.getId();
    }
}
