package me.github.fwfurtado.micro9313.user.api.listing;

import me.github.fwfurtado.micro9313.user.api.UserController;
import me.github.fwfurtado.micro9313.user.shared.UserView;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@UserController
class ListingController {

    private final ListingService service;

    ListingController(ListingService service) {
        this.service = service;
    }

    @GetMapping("{id}")
    Optional<UserView> showUser(@PathVariable Long id) {
        return service.showUserBy(id);
    }

}
