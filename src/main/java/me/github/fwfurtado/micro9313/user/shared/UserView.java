package me.github.fwfurtado.micro9313.user.shared;

public class UserView {
    private String name;
    private String email;

    UserView() {
    }

    public UserView(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }
    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "UserView{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
