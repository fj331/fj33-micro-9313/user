import org.springframework.cloud.contract.spec.Contract

Contract.make {
    label 'created_user'

    input {
        triggeredBy("fireEvent()")
    }

    outputMessage {
        sentTo('users')

        body(
                "id": 1,
                "name": "Fernando Furtado"
        )
    }
}