package me.github.fwfurtado.micro9313.user.api.registration;

import me.github.fwfurtado.micro9313.user.IntegrationTest;
import me.github.fwfurtado.micro9313.user.api.registration.RegistrationController.UserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;

@IntegrationTest
@AutoConfigureMessageVerifier
class RegistrationContractTest {
    private static final UserForm NEW_USER_FORM = new UserForm("fernando.furtado@caelum.com.br", "123456", "Fernando Furtado");

    @Autowired
    private RegistrationService service;

    public void fireEvent() {
        service.registerUserBy(NEW_USER_FORM);
    }
}